#include <iostream>

using namespace std;

class Vector
{
private:
	double x;
	double y;
	double z;

public:
	double GetX()
	{
		return x;
	}
	double GetY()
	{
		return y;
	}
	double GetZ()
	{
		return z;
	}
	void SetX(double ValueX)
	{
		x = ValueX;
	}
	void SetY(double ValueY)
	{
		y = ValueY;
	}
	void SetZ(double ValueZ)
	{
		z = ValueZ;
	}

	void LengthVector()
	{
		float Length = sqrt (pow (x, 2) + pow (y, 2) + pow (z, 2));
		cout << Length << endl;
	}

};

int main()
{
	Vector vecA;
	vecA.SetX(2);
	vecA.SetY(3);
	vecA.SetZ(5);
	vecA.LengthVector();

	return 0;
}
